package de.witterplay.si.tool;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.witterplay.si.core.Help;
import de.witterplay.si.player.Premium;
import de.witterplay.si.player.UUid;
import de.witterplay.si.server.Ram;

public class Tool implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("si")) {
		if (args.length == 0) {
			Help.help(sender);
			return true;
	}
		if ("info".equalsIgnoreCase(args[0])) {
			Help.info(sender);
			return true;
		}
		if ("help".equalsIgnoreCase(args[0])) {
			Help.help(sender);
			return true;
		}
		if ("uuid".equalsIgnoreCase(args[0])) {
			UUid.uuid(sender);
			return true;
		}
		if ("ram".equalsIgnoreCase(args[0])) {
			Ram.raminfo(sender);
			return true;
		}
		if ("premium".equalsIgnoreCase(args[0])) {
			if (args.length == 2) {
			Player p = (Player) sender;
			String player = args[1];
			Premium.ckeckp(p, player);
			return true;
			}else{
			Player p = (Player) sender;
			p.sendMessage("§a[SI] /si premium <name>");
			}
		}
}
		return false;

}
}
