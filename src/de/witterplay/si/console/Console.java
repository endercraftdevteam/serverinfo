package de.witterplay.si.console;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import de.witterplay.si.core.Help;
import de.witterplay.si.core.MainActivity;

public class Console implements CommandExecutor, Listener {
	

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		Player p = (Player) sender;
	if(MainActivity.plugin.getConfig().getBoolean("player-can-use-console") == true){
		if (cmd.getName().equalsIgnoreCase("c")) {
			if(p.isOp() == true){
			
			if (args.length >= 1) {
				StringBuilder builder = new StringBuilder();
				for(int i = 0 ; i < args.length ; i++)builder.append(args[i]).append(" ");

				
				MainActivity.plugin.getServer().dispatchCommand(MainActivity.plugin.getServer().getConsoleSender(), builder.toString());
				p.sendMessage("§a[SI]Der Befehl "+builder.toString()+"wurde über die CONSOLE ausgeführt");
				return true;
				}
		}
		if (args.length == 0) {
			Help.help(sender);
			return true;
	}
		return true;
}
		return false;

}
	return false;
}
}