package de.witterplay.si.core;



import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import de.witterplay.si.console.Console;
import de.witterplay.si.tool.Tool;



public class MainActivity extends JavaPlugin implements Listener {
	// start

	public Server server = getServer();
	public ConsoleCommandSender CONSOLE = server.getConsoleSender();
	public static Plugin plugin;                 

	

	public void onEnable() {
		plugin = this;
		Config.config();
		getCommand("si").setExecutor(new Tool());
		if(plugin.getConfig().getBoolean("player-can-use-console") == true){
		getCommand("c").setExecutor(new Console());
		}
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new Console(), this);
		CONSOLE.sendMessage("§6[SI] §2Erfolgreich geladen. Version:"
				+ plugin.getDescription().getVersion());

	}

	// stop
	public void onDisable() {
		CONSOLE.sendMessage("§6[SI] §2Erfolgreich gestopt");
	}

}