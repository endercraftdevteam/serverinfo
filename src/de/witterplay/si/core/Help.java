package de.witterplay.si.core;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class Help {


	public static void help(CommandSender sender) {
		Player p = (Player) sender;
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[Si]&c-#-#-Help-Server-Info-#-#-"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] /si help"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] /si info"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] /si uuid"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] /si ram"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] /si premium"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] /c"));
	}

	public static void info(CommandSender sender) {
		Player p = (Player) sender;
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&c[SI] ##################"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] Made by Witterplay"));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&a[SI] Version:"+MainActivity.plugin.getDescription().getVersion()));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&c[SI] ##################"));
	}
}

