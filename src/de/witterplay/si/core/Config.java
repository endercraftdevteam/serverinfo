package de.witterplay.si.core;

public class Config {
	public static void config(){

		MainActivity.plugin.getConfig().options().header(
				"#############################################" + "\n" +
				"#      - SERVER-INFO -        #" + "\n" +
				"#############################################" + "\n"+ "\n"
		);
		MainActivity.plugin.getConfig().addDefault("player-can-use-console", false);
		MainActivity.plugin.getConfig().options().copyHeader(true);
		MainActivity.plugin.getConfig().options().copyDefaults(true);
		MainActivity.plugin.saveConfig();
		MainActivity.plugin.reloadConfig();
		}
}
